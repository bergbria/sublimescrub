import sublime, sublime_plugin
import os, glob
import subprocess

def RunNscrub(view, nscrubVerb):
	FindNscrub()
	
	currentFilePath = view.window().active_view().file_name()
	currentDirectory = os.path.dirname(currentFilePath)

	if nscrubVerb == 'listproc':
		outputName = 'processDetails.json'
	else:
		outputName = 'ruleDetails.json'

	selectedText = GetCurrentlySelectedText(view)
	outputPath = os.path.join(currentDirectory, outputName)
	args = [nscrubVerb, '-r', selectedText, '-l', currentDirectory, '-o', outputPath]
	sublime.status_message("Running nscrub " + str(args))
	if RunNscrubProcess(args):
		view.window().open_file(outputPath)

def GetCurrentlySelectedText(view):
	selection = view.sel()[0]
	selectedText = view.substr(selection)
	return selectedText

def FindNscrub():
	cloudBuildToolsDirs = glob.glob(os.path.join(os.environ['NugetMachineInstallRoot'], 'office.cloudbuildtools*'))
	latestPackageDir = max(cloudBuildToolsDirs)
	nscrubPath = os.path.join(latestPackageDir, 'lib', 'net45', 'nscrub.exe')
	print(nscrubPath)
	return nscrubPath

def RunNscrubProcess(arguments, waitForExit = True):
	command = [FindNscrub()]
	command.extend(arguments)
	print("Executing: " + str(command))

	startupInfo = subprocess.STARTUPINFO()
	startupInfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW

	p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, startupinfo=startupInfo)

	if not waitForExit:
		print('not waiting for process exit')
		return True;

	print('waiting for process exit')
	out, err = p.communicate()
	exitCode = p.returncode
	out, err = out.decode("utf-8"), err.decode("utf-8")

	success = len(err) <= 0 and exitCode == 0
	if not success:
		sublime.status_message("Error running nscrub: " + err)
		print ('Nscub error: ' + err)

	return success

def UnescapePath(escapedPath):
	return escapedPath.replace('\\\\', '\\').strip('"')

class nscrub_listprocCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		RunNscrub(self.view, "listproc")

class nscrub_describeCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		RunNscrub(self.view, "describe")

class nscrub_open_ruleCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		selectedText = GetCurrentlySelectedText(self.view)
		args = ['open', '-r', selectedText]
		RunNscrubProcess(args, False)

class nscrub_open_sources_fileCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		selectedText = GetCurrentlySelectedText(self.view)
		args = ['open', '-s', '-r', selectedText]
		RunNscrubProcess(args, False)

class nscrub_open_escaped_pathCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		selectedText = GetCurrentlySelectedText(self.view)
		unescapedPath = UnescapePath(selectedText)
		print(unescapedPath)
		self.view.window().open_file(unescapedPath)

class nscrub_copy_unescaped_pathCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		selectedText = GetCurrentlySelectedText(self.view)
		unescapedPath = UnescapePath(selectedText)
		print(unescapedPath)
		sublime.set_clipboard(unescapedPath)
