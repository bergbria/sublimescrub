#SublimeSD

##Installation
1. Install Sublime Text 3 from https://www.sublimetext.com/3
2. Install the Package Control plugin from https://packagecontrol.io/installation
2. Bring up the command palette (ctrl+shift+p)
3. Add repository -> https://bitbucket.org/bergbria/sublimescrub
4. Bring up the command palette again
5. Install Package -> SublimeScrub

##Usage
1. Select the text you want to operate on
	1. Sublime's expand_selection command (ctrl+shift+space) is helpful here
2. Bring up the command palette (ctrl+shift+p)
3. Type in the command you want to run (e.g. nscrub listproc)
4. Press enter


##Known Limitations
1. Some of the nscrub commands (e.g. listproc and describe) don't function correctly without a loaded enlistment environment. You will likely want to start Sublime from an enlistment prompt.
2. There's not currently very much input validation or error handling.

##Bugs & Contributions
* If something doesn't work, check the Sublime console for any printed error messages (View -> Show Console)
* Contact bberger if you find any bugs or would like to contribute to the project